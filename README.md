# workshop-deep-learning-code

Outline of a half-day workshop in which we explore a deep neural network through Python code.

1. Install and run [Nordic runes example](https://gitlab.com/brohrer/study-norse-runes/-/blob/master/study-norse-runes.ipynb)
2. Tour of a framework
3. Computation graphs
4. A dense neural network layer
5. Backpropagation
6. Optimizers
7. Exercise: modify learning rate
8. Exercise: experiment with optimizers
6. Regularizers
7. Exercise: experiment with regularizers
7. Dropout
8. Exercise: modify dropout
7. Initializers and normalization
8. Exercise: experiment with initializers
8. Architecture
9. Exercise: experiment with architectures
10. Data preprocessing
10. Exercise: change data set
11. Hyperparameter tuning
10. Exercise: optimize hyperparameters  
